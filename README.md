# ITensor_MPDO
This code performs real-time evolution of a density matrix in a MPDO form (only 1D spin-1/2 chains).

The model used can be found [here](https://arxiv.org/abs/2006.13891).

To simulate the next-to-nearest neighbor interaction, I implemented a 3-site TEBD routine. This presents significant speed-ups comparing to the swap gate method.

To run the code you must have a working ITensor version and run: `make app=tevol && ./tevol`
