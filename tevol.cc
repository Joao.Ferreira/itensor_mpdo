//	Time-evolution of density matrices using TEBD with 3-site gates
//	For any doubt please contact joao.ferreira@unige.ch
//	If you wish to use this code, consider citing  https://arxiv.org/abs/2006.13891

#include "itensor/all.h"
#include "3TEBD.h"
#include "Z4.h"

using namespace itensor;
using namespace std;

#define PI 3.14159265

int main(int argc, char** argv){

	//Create Siteset of Pauli matrices with 8 sites
	int N=5;
	auto sites=Z4(N);

	//Create left and right eigenvectors (matrices) of the Liouvillian
	//Initialize them to identity (normalization will be corrected in the end)
	auto state = InitState(sites);
	for(int i=1;i<=N;i++)
	state.set(i,"i");

	auto id=MPS(state);
	auto ness=MPS(state);

	//Create the bond gates to perform time evolution
	//Model: https://arxiv.org/abs/2006.13891
	//Parameters: Delta=1 V=0.5 delta_mu=0.1
	//Note: The next-to-nearest neighbor interaction benefits
	//from applying a 3-site gate instead of the swap technique

	//To simulate a super operator L acting left and right of a matrix rho
	//e.g L(rho)=A.rho.B,  we use the notation sites.op("A|B",i)

	vector<BondGate3> gates;
	double dm=0.1;
	double V=0.5;
	double tstep=0.01;

	//Right Sweep
	for(int j=1;j<=N-2;++j){
			auto ll=ii*(sites.op("|Sx",j)*sites.op("|Sx",j+1)+sites.op("|Sy",j)*sites.op("|Sy",j+1)+sites.op("|Sz",j)*sites.op("|Sz",j+1))*sites.op("|",j+2);
			ll-=ii*(sites.op("Sx|",j)*sites.op("Sx|",j+1)+sites.op("Sy|",j)*sites.op("Sy|",j+1)+sites.op("Sz|",j)*sites.op("Sz|",j+1))*sites.op("|",j+2);
			ll+=V*ii*sites.op("|Sz",j)*sites.op("|",j+1)*sites.op("|Sz",j+2);
			ll-=V*ii*sites.op("Sz|",j)*sites.op("|",j+1)*sites.op("Sz|",j+2);

			if(j==1){
				ll+=(1+dm/2.)*(2.*sites.op("S+|S-",1)-sites.op("S-S+|",1)-sites.op("|S-S+",1))*sites.op("|",2)*sites.op("|",3);
				ll+=(1-dm/2.)*(2.*sites.op("S-|S+",1)-sites.op("S+S-|",1)-sites.op("|S+S-",1))*sites.op("|",2)*sites.op("|",3);
			}
			else if(j==N-2){
				ll+=(1-dm/2.)*(2.*sites.op("S+|S-",N)-sites.op("S-S+|",N)-sites.op("|S-S+",N))*sites.op("|",N-1)*sites.op("|",N-2);
				ll+=(1+dm/2.)*(2.*sites.op("S-|S+",N)-sites.op("S+S-|",N)-sites.op("|S+S-",N))*sites.op("|",N-1)*sites.op("|",N-2);

				ll+=ii*(sites.op("|Sx",N-1)*sites.op("|Sx",N)+sites.op("|Sy",N-1)*sites.op("|Sy",N)+sites.op("|Sz",N-1)*sites.op("|Sz",N))*sites.op("|",N-2);
				ll-=ii*(sites.op("Sx|",N-1)*sites.op("Sx|",N)+sites.op("Sy|",N-1)*sites.op("Sy|",N)+sites.op("Sz|",N-1)*sites.op("Sz|",N))*sites.op("|",N-2);
			}
			//The "right" flag at the end indicates that we are doing a right sweep
			auto bg = BondGate3(sites,j,j+1,j+2,BondGate3::tImag,-tstep/2,ll,"right");
			gates.push_back(bg);
	}

	//Left Sweep
	for(int j=N-2;j>=1;--j){
			auto ll=ii*(sites.op("|Sx",j)*sites.op("|Sx",j+1)+sites.op("|Sy",j)*sites.op("|Sy",j+1)+sites.op("|Sz",j)*sites.op("|Sz",j+1))*sites.op("|",j+2);
			ll-=ii*(sites.op("Sx|",j)*sites.op("Sx|",j+1)+sites.op("Sy|",j)*sites.op("Sy|",j+1)+sites.op("Sz|",j)*sites.op("Sz|",j+1))*sites.op("|",j+2);
			ll+=V*ii*sites.op("|Sz",j)*sites.op("|",j+1)*sites.op("|Sz",j+2);
			ll-=V*ii*sites.op("Sz|",j)*sites.op("|",j+1)*sites.op("Sz|",j+2);

			if(j==1){
				ll+=(1+dm/2.)*(2.*sites.op("S+|S-",1)-sites.op("S-S+|",1)-sites.op("|S-S+",1))*sites.op("|",2)*sites.op("|",3);
				ll+=(1-dm/2.)*(2.*sites.op("S-|S+",1)-sites.op("S+S-|",1)-sites.op("|S+S-",1))*sites.op("|",2)*sites.op("|",3);
			}
			else if(j==N-2){
				ll+=(1-dm/2.)*(2.*sites.op("S+|S-",N)-sites.op("S-S+|",N)-sites.op("|S-S+",N))*sites.op("|",N-1)*sites.op("|",N-2);
				ll+=(1+dm/2.)*(2.*sites.op("S-|S+",N)-sites.op("S+S-|",N)-sites.op("|S+S-",N))*sites.op("|",N-1)*sites.op("|",N-2);

				ll+=ii*(sites.op("|Sx",N-1)*sites.op("|Sx",N)+sites.op("|Sy",N-1)*sites.op("|Sy",N)+sites.op("|Sz",N-1)*sites.op("|Sz",N))*sites.op("|",N-2);
				ll-=ii*(sites.op("Sx|",N-1)*sites.op("Sx|",N)+sites.op("Sy|",N-1)*sites.op("Sy|",N)+sites.op("Sz|",N-1)*sites.op("Sz|",N))*sites.op("|",N-2);
			}
			auto bg = BondGate3(sites,j,j+1,j+2,BondGate3::tImag,-tstep/2,ll,"left");
			gates.push_back(bg);
	}

	//Time-evolve the density matrix
	//Use the Normalize flag to avoid growing matrix elements
	gateTEvolMixed(gates,100,tstep,ness,{"MaxDim=",80,"Verbose=",false,"Normalize=",true});

	//Create the total spin current operator
	AutoMPO ampo(sites);
	for(int i=1;i<=N-1;++i){
	ampo += 2.,"Sx|",i,"Sy|",i+1;
	ampo += -2.,"Sy|",i,"Sx|",i+1;
	}
	auto Js=toMPO(ampo);

	Cplx jj=innerC(id,Js,ness)/innerC(id,ness);
	printfln("\nSpin current density= %.4f",jj.real()/(N-1));

	return 0;
}
