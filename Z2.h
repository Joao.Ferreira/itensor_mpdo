//Redefinition of the Z2 siteset to include Pauli matrices
//and not only spin matrices
#pragma once

#include "itensor/mps/siteset.h"
#include "itensor/util/str.h"

namespace itensor {

class Z2Site;

using Z2 = BasicSiteSet<Z2Site>;

class Z2Site
    {
    Index s;
    public:

    Z2Site(Index const& I) : s(I) { }

    Z2Site(Args const& args = Args::global())
        {
        auto ts = TagSet("Site,S=1/2");
        if( args.defined("SiteNumber") )
          ts.addTags("n="+str(args.getInt("SiteNumber")));
        auto conserveqns = args.getBool("ConserveQNs",false);
        auto conserveSz = args.getBool("ConserveSz",conserveqns);
        auto conserveParity = args.getBool("ConserveParity",false);
        if(conserveSz && conserveParity)
            {
            s = Index(QN({"Sz",+1},{"Parity",1,2}),1,
                      QN({"Sz",-1},{"Parity",0,2}),1,Out,ts);
            }
        else if(conserveSz)
            {
            s = Index(QN({"Sz",+1}),1,
                      QN({"Sz",-1}),1,Out,ts);
            }
        else if(conserveParity)
            {
            s = Index(QN({"Parity",1,2}),1,
                      QN({"Parity",0,2}),1,Out,ts);
            }
        else
            {
            s = Index(2,ts);
            }
        }

    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
        if(state == "Up")
            {
            return s(1);
            }
        else
        if(state == "Dn")
            {
            return s(2);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args = Args::global()) const
        {
        auto sP = prime(s);

        auto Up = s(1);
        auto UpP = sP(1);
        auto Dn = s(2);
        auto DnP = sP(2);

        auto Op = ITensor(dag(s),sP);
        double pf=1.;
        if(args.getBool("PauliMatrices",true))
          pf=2.;

        if(opname == "Sz")
            {
            Op.set(Up,UpP,+0.5*pf);
            Op.set(Dn,DnP,-0.5*pf);
            }
        else
        if(opname == "Sx")
            {
            //if(not hasQNs(s))
            //    {
                Op.set(Up,DnP,+0.5*pf);
                Op.set(Dn,UpP,+0.5*pf);
            //    }
            //else
            //    {
            //    Error("Operator " + opname + " does not have a well defined QN flux");
            //    }
            }
        else
        if(opname == "ISy")
            {
            //if(not hasQNs(s))
            //    {
                Op.set(Up,DnP,-0.5*pf);
                Op.set(Dn,UpP,+0.5*pf);
            //    }
            //else
            //    {
            //    Error("Operator " + opname + " does not have a well defined QN flux");
            //    }
            }
        else
        if(opname == "Sy")
            {
            //if(not hasQNs(s))
            //    {
                Op.set(Up,DnP,+0.5*Cplx_i*pf);
                Op.set(Dn,UpP,-0.5*Cplx_i*pf);
            //    }
            //else
            //    {
            //    Error("Operator " + opname + " does not have a well defined QN flux");
            //    }
            }
        else
        if(opname == "Sp" || opname == "S+")
            {
            Op.set(Dn,UpP,1);
            }
        else
        if(opname == "Sm" || opname == "S-")
            {
            Op.set(Up,DnP,1);
            }
        else
        if(opname == "projUp" || opname == "S+S-")
            {
            Op.set(Up,UpP,1);
            }
        else
        if(opname == "projDn"|| opname == "S-S+")
            {
            Op.set(Dn,DnP,1);
            }
        else
        if(opname == "S2")
            {
            Op.set(Up,UpP,0.75*pf*pf);
            Op.set(Dn,DnP,0.75*pf*pf);
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

    //
    // Deprecated, for backwards compatibility
    //

    Z2Site(int n, Args const& args = Args::global())
        {
        *this = Z2Site({args,"SiteNumber=",n});
        }

    };

} //namespace itensor
