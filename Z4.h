
#pragma once

#include "itensor/mps/siteset.h"
#include "itensor/util/str.h"
#include "Z2.h"

using namespace std;
namespace itensor {
auto ii=1.0*Cplx_i;

class Z4Site;

using Z4 = BasicSiteSet<Z4Site>;

class Z4Site
    {
    Index s;
    std::vector<std::string> paulibasis{"Id","Sx","Sy","Sz"};
    public:

    Z4Site(Index I) : s(I) { }

    Z4Site(Args const& args = Args::global())
        {
        auto ts = TagSet("Site,Z4");
        if( args.defined("SiteNumber") )
          ts.addTags("n="+str(args.getInt("SiteNumber")));
        s = Index(4,ts);
        }
    std::vector<std::string>
    basis() const {return paulibasis;}

    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
          if(state == "i") { return s(1); }
          else
          if(state == "x") { return s(2); }
          else
          if(state == "y") { return s(3); }
          else
          if(state == "z") { return s(4); }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor //The operators must read A|B whose action is \sigma_A \rho \sigma_B
	op(std::string const& opname,
	   Args const& args) const
        {
          auto s2 = Z2(1); //Auxiliar siteset

          std::size_t sep = opname.find("|");
          if (sep==std::string::npos)
            Error("Operator "+opname+" not recognized");
          std::string opleft (opname, 0, sep);
          std::string opright (opname, sep+1,opname.size()-sep-1);
          if (opleft.empty())
            opleft="Id";
          if (opright.empty())
            opright="Id";
          auto sl=s2.op(opleft,1,{"PauliMatrices=",true});
          auto sr=s2.op(opright,1,{"PauliMatrices=",true});

        auto sP = prime(s);
        auto Op = ITensor(dag(s),sP);

        for(int i: {0,1,2,3}){
          for(int j: {0,1,2,3}){
            auto sj=s2.op(paulibasis[j],1,{"PauliMatrices=",true});
            auto si=s2.op(paulibasis[i],1,{"PauliMatrices=",true});
            auto ax=multSiteOps(si,multSiteOps(sl,multSiteOps(sj,sr)));
            ax*=delta(s2.si(1),prime(s2.si(1)));
            Op.set(sP(i+1),s(j+1),ax.cplx()/2.);
          }
        }

        return Op;
        }

    Z4Site(int n, Args const& args = Args::global())
        {
        *this = Z4Site({args,"SiteNumber=",n});
        }

    };

} //namespace itensor
